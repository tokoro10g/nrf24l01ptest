SHELL = /bin/sh
TARGET_ARCH   = -mcpu=cortex-m3 -mthumb
INCLUDE_DIRS  = -I ../../stm32plus/lib/include \
				-I ../../stm32plus/lib \
				-I ../../stm32plus/lib/include/stl \
				-I ../../Libraries/CMSIS/Device/ST/STM32F10x/ \
				-I ../../Libraries/CMSIS/Include
STARTUP_DIR = ./system/f1mdvl/
BOARD_OPTS = -DHSE_VALUE=\(\(uint32_t\)8000000\) -DSTM32F10X_MD_VL
FIRMWARE_OPTS = -DSTM32PLUS_F1_MD_VL
COMPILE_OPTS  = -std=gnu++11 -O0 -g3 -ffunction-sections -fpermissive -fdata-sections -fsigned-char -fno-rtti -fexceptions -Wall -fmessage-length=0 $(INCLUDE_DIRS) $(BOARD_OPTS) $(FIRMWARE_OPTS)

TOOLDIR = /opt/cross/arm-none-eabi-x-tools/bin/
CC      = $(TOOLDIR)arm-none-eabi-g++
CXX	= $(CC)
AS      = $(CC)
LD      = $(CC)
AR      = $(TOOLDIR)arm-none-eabi-ar
OBJCOPY = $(TOOLDIR)arm-none-eabi-objcopy
CFLAGS  = $(COMPILE_OPTS)
CXXFLAGS= $(COMPILE_OPTS)
ASFLAGS = -x assembler-with-cpp -c $(TARGET_ARCH) $(COMPILE_OPTS) 
LDFLAGS = -Wl,--gc-sections,-Map=bin/main.map,-cref -T $(STARTUP_DIR)Linker.ld $(INCLUDE_DIRS) -lstm32plus-master-f1mdvl-debug -L ../../stm32plus/lib/build/debug-f1mdvl-8000000 -nostartfiles

all: startup bin/main.hex

# main.o is compiled by suffix rule automatucally
bin/main.hex: $(patsubst %.c,%.o,$(wildcard *.c)) $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(STARTUP_DIR)Startup.o system/LibraryHacks.o ../../stm32plus/lib/build/debug-f1mdvl-8000000/libstm32plus-master-f1mdvl-debug.a 
	$(LD) $(LDFLAGS) $(TARGET_ARCH) $^ -o bin/main.elf 
	$(OBJCOPY) -O ihex bin/main.elf bin/main.hex

startup:
	$(AS) -o $(STARTUP_DIR)Startup.o $(ASFLAGS) $(STARTUP_DIR)Startup.asm

clean:
	rm -rf *.o *.s bin\*
	
debug:

#flash:
#	start /WAIT C:\"Program Files (x86)"\STMicroelectronics\Software\"Flash Loader Demonstrator"\STMFlashLoader.exe -c --pn 10 --br 115200 --to 5000 -i STM32F4_1024K -e --sec 7 0 1 2 3 4 5 6 -d --fn bin\main.hex
#	start C:\"Program Files (x86)"\teraterm\ttermpro.exe /C=10 /BAUD=921600
