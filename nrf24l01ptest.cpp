#include "config/stm32plus.h"
#include "config/gpio.h"
#include "config/spi.h"
#include "config/usart.h"
#include "config/timing.h"

using namespace stm32plus;

template<class TSpi> 
class nRF24L01P : public TSpi{
public:
	enum{
		REG_CONFIG=0x00,
		REG_EN_AA=0x01,
		REG_EN_RXADDR=0x02,
		REG_SETUP_AW=0x03,
		REG_SETUP_RETR=0x04,
		REG_RF_CH=0x05,
		REG_RF_SETUP=0x06,
		REG_STATUS=0x07,
		REG_OBSERVE_TX=0x08,
		REG_CD=0x09,
		REG_RX_ADDR_P0=0x0a,
		REG_RX_ADDR_P1=0x0b,
		REG_RX_ADDR_P2=0x0c,
		REG_RX_ADDR_P3=0x0d,
		REG_RX_ADDR_P4=0x0e,
		REG_RX_ADDR_P5=0x0f,
		REG_TX_ADDR=0x10,
		REG_RX_PW_P0=0x11,
		REG_RX_PW_P1=0x12,
		REG_RX_PW_P2=0x13,
		REG_RX_PW_P3=0x14,
		REG_RX_PW_P4=0x15,
		REG_RX_PW_P5=0x16,
		REG_FIFO_STATUS=0x17,
		REG_DYNPD=0x1c,
		REG_FEATURE=0x1d,
	};
	enum{
		CMD_R_REGISTER=0x00,
		CMD_W_REGISTER=0x20,
		CMD_R_RX_PAYLOAD=0x61,
		CMD_W_TX_PAYLOAD=0xa0,
		CMD_FLUSH_TX=0xe1,
		CMD_FLUSH_RX=0xe2,
		CMD_REUSE_TX_PL=0xe3,
		CMD_R_RX_PL_WID=0x60,
		CMD_W_ACK_PAYLOAD=0xa8,
		CMD_W_TX_PAYLOAD_NOACK=0xd0,
		CMD_NOP=0xFF
	};
	enum{
		STATUS_RX_DR=0x40,
		STATUS_TX_DS=0x20,
		STATUS_MAX_RT=0x10
	};
	nRF24L01P(typename TSpi::Parameters& params,Gpio& _ce,Gpio& _csn):TSpi(params),ce(_ce),csn(_csn){
		MillisecondTimer::initialise();
		setCEState(false);
		setCSNState(true);
		MillisecondTimer::delay(5);
		writeReg(REG_EN_AA,0x01);
		writeReg(REG_RF_CH,40);
		writeReg(REG_RF_SETUP,0x00);
	}
	void setCEState(bool state){
		ce.setState(state);
	}
	void setCSNState(bool state){
		csn.setState(state);
	}
	void writeCmd(uint8_t reg,uint8_t val){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		while(!this->readyToSend());
		this->send(&val,1);
		setCSNState(true);
	}
	void writeCmd(uint8_t reg,uint8_t *buf,uint8_t len){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		while(!this->readyToSend());
		this->send(buf,len);
		setCSNState(true);
	}
	inline void writeReg(uint8_t reg,uint8_t val){
		reg&=0x1f;
		reg|=0x20;
		writeCmd(reg,val);
	}
	inline void writeReg(uint8_t reg,uint8_t *buf,uint8_t len){
		reg&=0x1f;
		reg|=0x20;
		writeCmd(reg,buf,len);
	}
	void readCmd(uint8_t reg,uint8_t *buf,uint8_t len){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		this->receive(buf,len);
		setCSNState(true);
	}
	uint8_t readCmd(uint8_t reg){
		setCSNState(false);
		while(!this->readyToSend());
		this->send(&reg,1);
		uint8_t resp;
		this->receive(&resp,1);
		setCSNState(true);
		return resp;
	}
	inline void readReg(uint8_t reg,uint8_t *buf,uint8_t len){
		reg&=0x1f;
		readCmd(reg,buf,len);
	}
	inline uint8_t readReg(uint8_t reg){
		reg&=0x1f;
		return readCmd(reg);
	}
	void selectRxMode(uint8_t *address,uint8_t len){
		setCEState(false);
		writeReg(REG_RX_ADDR_P0,address,5);
		writeReg(REG_RX_PW_P0,len);
		writeReg(REG_CONFIG,0x0f);
		setCEState(true);
	}
	void selectTxMode(uint8_t *address,uint8_t *buf,uint8_t len){
		setCEState(false);
		writeReg(REG_TX_ADDR,address,5);
		writeReg(REG_RX_ADDR_P0,address,5);
		writeCmd(CMD_W_TX_PAYLOAD,buf,len);
		writeReg(REG_EN_RXADDR,0x01);
		writeReg(REG_SETUP_RETR,0x1a);
		writeReg(REG_CONFIG,0x0e);
		setCEState(true);
	}
	void clearFlag(){
		writeReg(REG_STATUS,0xff);
	}
	uint8_t getStatus(){
		uint8_t status=readReg(REG_STATUS);
		clearFlag();
		return status;
	}
private:
	Gpio& ce;
	Gpio& csn;
};

int main() {
	GpioA<DefaultDigitalInputFeature<0> > gpioa;
	GpioB<DefaultDigitalInputFeature<0> > gpiob;
	GpioB<DefaultDigitalOutputFeature<1> > csnPort;
	GpioC<DefaultDigitalOutputFeature<3> > cePort;
	GpioC<DefaultDigitalOutputFeature<8,9> > gpioc;

	Spi1<>::Parameters params;
	params.spi_mode=SPI_Mode_Master;
	params.spi_baudRatePrescaler=SPI_BaudRatePrescaler_8;
	params.spi_cpol=SPI_CPOL_Low;
	params.spi_cpha=SPI_CPHA_1Edge;
	nRF24L01P<Spi1<> > nrf(params,cePort[3],csnPort[1]);

	Usart2<> usart2(115200);
	UsartPollingOutputStream _usartos(usart2);
	TextOutputStream os2(_usartos);

	if(nrf.readReg(nrf.REG_RX_ADDR_P5)==0xc6){
		gpioc[8].set();
	}
	//nrf.writeReg(0x02, 0x4f);
	
	uint8_t address[5]={0x34,0x43,0x10,0x10,0x01};
	nrf.selectRxMode(address,1);
	uint8_t testAddress[5];
	nrf.readReg(nrf.REG_RX_ADDR_P0, testAddress, 5);
	os2<<(uint16_t)testAddress[0]<<","<<(uint16_t)testAddress[1]<<","<<(uint16_t)testAddress[2]<<","<<(uint16_t)testAddress[3]<<","<<(uint16_t)testAddress[4]<<","<<(uint16_t)testAddress[5]<<"\r\n";
	
	bool oldState[2]={0,1};
	nrf.writeCmd(nrf.CMD_FLUSH_TX, 0xff);
	nrf.writeCmd(nrf.CMD_FLUSH_TX, 0xff);
	nrf.writeCmd(nrf.CMD_FLUSH_TX, 0xff);
	nrf.writeCmd(nrf.CMD_FLUSH_RX, 0xff);
	nrf.writeCmd(nrf.CMD_FLUSH_RX, 0xff);
	nrf.writeCmd(nrf.CMD_FLUSH_RX, 0xff);
	while(1){
		uint8_t txstatus=nrf.readReg(nrf.REG_OBSERVE_TX);
		uint8_t status=nrf.readReg(nrf.REG_STATUS);
		os2<<(uint16_t)((txstatus&0xF0)>>4)<<"\t"<<(uint16_t)(txstatus&0x0F)<<"\r\n";
		os2<<(uint16_t)status<<"\r\n";
		uint8_t inputState[2];
		inputState[0]=gpioa[0].read();
		inputState[1]=gpiob[0].read();
		if((bool)inputState[0]!=oldState[0]){
			// Button B1 pushed
			// Transmit the button state
			nrf.selectTxMode(address, inputState, 1);
		}
		if(inputState[1]==0){
			// IRQ from RF module
			uint8_t status=nrf.getStatus();
			if(status&nrf.STATUS_RX_DR){
				uint8_t recv = nrf.readCmd(nrf.CMD_R_RX_PAYLOAD);
				if(recv==0x0){
					gpioc[9].reset();
				} else if(recv==0x1){
					gpioc[9].set();
				}
			}
			if(status&nrf.STATUS_TX_DS){
				nrf.selectRxMode(address,1);
			}
			if(status&nrf.STATUS_MAX_RT){
				nrf.writeCmd(nrf.CMD_FLUSH_TX, 0);
				nrf.selectRxMode(address,1);
			}
		}
		oldState[0]=inputState[0];
		oldState[1]=inputState[1];
		MillisecondTimer::delay(20);
	}

	return 0;
}
